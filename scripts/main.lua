

--load stuff
env.info("--------------- Loading Stuff -----------------")
--missionScriptPath = lfs.writedir()..'\\Missions\\TKO\\scripts\\'

-- set up path
env.info(package.path)
package.path = package.path..";"..lfs.writedir().."\\Missions\\TKO\\scripts\\?.lua;"
env.info(package.path)

local function loadMIST ()
  --assert(loadfile(missionScriptPath.."mist_ko.lua"))()
  --assert(loadfile(missionScriptPath.."mist_4_3_74.lua"))() -- Upgraded edition
  require("mist_4_3_74")
end

local function loadCTLD ()
  -- assert(loadfile(missionScriptPath.."CTLD.lua"))() -- Need to be fixed, to populate meny.
  --assert(loadfile(missionScriptPath.."CTLD_ko.lua"))()
  require("CTLD_ko")
end

local function loadkoCSAR ()
  --assert(loadfile(missionScriptPath.."CSAR.lua"))()
  require("ko_CSAR")
end

local function loadKaukasusOffensive ()
  require("ko_data")
  require("ko_Scoreboard")
  require("ko_TCPSocket")
  --assert(loadfile(missionScriptPath.."EWRS.lua"))()
  --assert(loadfile(missionScriptPath.."ko_Engine2.lua"))()
  require("ko_Engine2")
end

local function loadWitchcraft ()
  env.info("Starting Witchcraft")
  if witchcraft then
    witchcraft.start(_G)
  end
end

loadMIST ()

env.info("scheduling functions")
mist.scheduleFunction(loadCTLD,{}, timer.getTime() + 2)
mist.scheduleFunction(loadkoCSAR,{}, timer.getTime() + 3)
mist.scheduleFunction(loadKaukasusOffensive,{}, timer.getTime() + 4)
mist.scheduleFunction(loadWitchcraft, {}, timer.getTime() + 6)

local runDebugger = false
if runDebugger then
	env.info("attempting to load debugger")
	  --local json = require 'JSON'
	--local json = loadfile(missionScriptPath.."dkjson.lua")()
	--assert(loadfile(missionScriptPath.."vscode-debuggee.lua"))()  -- called instead of --
	local json = require("dkjson")
	local debuggee = require("vscode-debuggee")
	local startResult, breakerType = debuggee.start(json)
	--print('debuggee start ->', startResult, breakerType)
	env.info('debuggee start -> ' .. tostring(startResult) .. ", " .. breakerType)

	-- poll debugger
	function PollDebugger()
		--env.info("poll")
		timer.scheduleFunction(PollDebugger, {}, timer.getTime() + 0.001)	--reschedule first in case of Lua error
		debuggee.poll()
	end

	timer.scheduleFunction(PollDebugger, {}, timer.getTime() + 0.001)	--reschedule first in case of Lua error
end