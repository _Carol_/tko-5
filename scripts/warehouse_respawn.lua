local spawnTable = {
	["heading"] = 0,
	["route"] = 
	{
		["points"] = 
		{
			[1] = 
			{
				["alt"] = 0,
				["type"] = "",
				["name"] = "",
				["y"] = 648256.47522547,
				["speed"] = 0,
				["x"] = -292348.24061614,
				["formation_template"] = "",
				["action"] = "",
			}, -- end of [1]
		}, -- end of ["points"]
	}, -- end of ["route"]
	["units"] = 
	{
		[1] = 
		{
			["category"] = "Warehouses",
			["shape_name"] = "SkladC",
			["type"] = ".Ammunition depot",
			["rate"] = 100,
			["y"] = 648256.47522547,
			["x"] = -292348.24061614,
			["name"] = "Bunker",
			["heading"] = 0,
		}, -- end of [1]
	}, -- end of ["units"]
	["y"] = 648256.47522547,
	["x"] = -292348.24061614,
	["name"] = "Bunker",
	["linkOffset"] = false,
	["dead"] = false,
} -- end of [1]


mist.dynAdd(mist.utils.deepCopy(spawnTable))
